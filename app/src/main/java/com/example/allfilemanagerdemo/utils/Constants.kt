package com.example.allfilemanagerdemo.utils

import android.Manifest

/**
 *@author:njb
 *@date: 2023/12/5 17:12
 *@desc:常量工具类
 */
class Constants {
    companion object {
        const val REQUEST_CODE_PERMISSIONS = 101
        const val REQUEST_CODE_CAMERA = 102
        const val REQUEST_CODE_CROP = 103
        const val REQUEST_CODE_AUDIO = 104

        const val DATE_FORMAT = "yyyy-MM-dd HH.mm.ss"
        const val PHOTO_EXTENSION = ".jpg"
        const val FILE_CHOOSER_RESULT_CODE = 105

        val REQUIRED_PERMISSIONS = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
        )
    }
}